<?php

namespace Drupal\timemodule;

/**
 * Class HelloMessage
 *
 * @package Drupal\timemodule
 */
class HelloMessage {

  // Массив с сообщениями.
  private $message;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    // Записываем сообщения в свойство.
    $this->setMessage();
  }

  /**
   * Здесь мы задаем варианты сообщений.
   */
  private function setMessage() {
    $user = \Drupal::currentUser()->id();
    $this->message = 'Hi your uid is ' . $user . " and it is "  . date('l jS \of F Y H:i:s') . " this message works via a service";
  }

  /**
   * Метод, который возвращает
   */
  public function getMessage() {
    return $this->message;
  }

}