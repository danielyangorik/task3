<?php

namespace Drupal\timemodule\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello Block.
 * 
 * @Block(
 *   id = "hello_block",
 *   admin_label = @Translation("Hello block"),
 *   category = @Translation("Time Module"),
 * )
 */



class HelloBlock extends BlockBase {
	
  

  public function build() {
    $user = \Drupal::currentUser()->id();
    /**
     * {@inheritdoc}
     */
    return[

      '#markup' => $this->t('Hi ' . $user . "<br />" . date('l jS \of F Y H:i:s')),
      '#cache' => [
        'contexts' => [
          'user',
        ],
      ]

    ];

  }

}
